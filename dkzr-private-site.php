<?php
/**
 * Plugin Name: Private Site
 * Plugin URI: https://gitlab.com/dkzr-wordpress/plugins/dkzr-private-site
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/private-site
 * Description: Close site for all but selected logged-in users or excluded IPs.
 * Version: 3.4
 * Author: Joost de Keijzer <j@dkzr.nl>
 * Text Domain: dkzr-private-site
 * Domain Path: languages
 * License: GPL2
 */

class dkzrPrivateSite {
  public function __construct() {
    add_action( 'wp_loaded', [ $this, 'wp_loaded' ], 0 );

    add_action( 'init', [ $this, 'init' ] );
    add_action( 'admin_init', [ $this, 'admin_init' ] );
    add_action( 'admin_menu', [ $this, 'admin_menu' ] );
  }

  public function admin_menu() {
    add_options_page(
      __( 'Private Site settings', 'dkzr-private-site' ),
      __( 'Private Site', 'dkzr-private-site' ),
      'manage_options',
      'dkzr-private-site',
      [ $this, 'setting_page' ]
    );
  }

  public function init() {
    register_setting(
      'dkzr-private-site',
      'dkzr_private_site_frontend_roles',
      [
        'type' => 'array',
        'default' => [],
      ]
    );
    register_setting(
      'dkzr-private-site',
      'dkzr_private_site_backend_roles',
      [
        'type' => 'array',
        'default' => [],
      ]
    );


    register_setting(
      'dkzr-private-site',
      'dkzr_private_site_exclude_ips',
      [
        'type' => 'string',
        'default' => implode( "\n", [ '127.0.0.1 # IPv4 localhost', '::1       # IPv6 localhost' ] ),
      ]
    );
    register_setting(
      'dkzr-private-site',
      'dkzr_private_site_exclude_uris',
      [
        'type' => 'string',
        'default' => implode( "\n", [ '# redirects to /wp-login.php and /wp-admin/', '/login', '/admin' ] ),
      ]
    );
  }

// TODO: add backend_roles checks
  public function admin_init() {
    add_settings_section(
      'dkzr_private_site_settings_exclude_ips',
      __( 'Exclude IPs', 'dkzr-private-site' ),
      function() { echo wpautop( __( 'List the IP addresses with full site access. Applies to frontend, AJAX, JSON and XML-RPC requests.', 'dkzr-private-site' ) ); },
      'dkzr-private-site'
    );

    add_settings_field(
      'exclude_ips',
      __( 'IPs with acces to site', 'dkzr-private-site' ),
      [ $this, 'setting_field_exclude_ips' ],
      'dkzr-private-site',
      'dkzr_private_site_settings_exclude_ips'
    );

    add_settings_section(
      'dkzr_private_site_settings_exclude_uris',
      __( 'Exclude URIs', 'dkzr-private-site' ),
      function() { echo wpautop( __( 'List frontend URIs that are accessable to all visitors.', 'dkzr-private-site' ) ); },
      'dkzr-private-site'
    );

    add_settings_field(
      'exclude_uris',
      __( 'Accessable URIs', 'dkzr-private-site' ),
      [ $this, 'setting_field_exclude_uris' ],
      'dkzr-private-site',
      'dkzr_private_site_settings_exclude_uris'
    );

    add_settings_section(
      'dkzr_private_site_settings_roles',
      __( 'Allow roles', 'dkzr-private-site' ),
      function() { echo wpautop( __( 'Select user roles with frontend access. AJAX and JSON requests require any valid login. XLM-RPC requests are blocked unless allowed by IP.', 'dkzr-private-site' ) ); },
      'dkzr-private-site'
    );

    add_settings_field(
      'allow_roles',
      __( 'Frontend roles', 'dkzr-private-site' ),
      [ $this, 'setting_field_roles' ],
      'dkzr-private-site',
      'dkzr_private_site_settings_roles'
    );
  }

  public function setting_page() {
    global $title, $plugin_page;
?>
<div class="wrap">
<h1><?php echo esc_html( $title ); ?></h1>

<form method="post" action="options.php" novalidate="novalidate">

<?php settings_fields( $plugin_page ); ?>

<?php do_settings_sections( $plugin_page ); ?>

<?php submit_button(); ?>
</form>

</div><!-- .wrap -->
<?php
  }

  public function setting_field_roles() {
    global $wp_roles;

    $setting = (array) get_option( 'dkzr_private_site_frontend_roles' );

    echo wpautop( __( 'Select the user roles that are allowed to view this site', 'dkzr-private-site' ) );
    foreach( $wp_roles->roles as $label => $role ) {
      if ( 'administrator' == $label ) {
      printf( '<label><input type="checkbox" name="dkzr_private_site_frontend_roles[]" value="%s" disabled=disabled %s>%s</label><br>', $label, checked( true, true, false ), $role['name'] );
      } else {
        printf( '<label><input type="checkbox" name="dkzr_private_site_frontend_roles[]" value="%s" %s>%s</label><br>', $label, checked( in_array( $label, $setting, true ), true, false ), $role['name'] );
      }
    }
    echo wpautop( __( 'Administrator users are always allowed to access the site.', 'dkzr-private-site' ) );
  }

  public function setting_field_exclude_ips() {
    $setting = get_option( 'dkzr_private_site_exclude_ips' );

    echo wpautop( __( 'Add one IP per line, comments start with "#"', 'dkzr-private-site' ) );
    printf( '<textarea name="dkzr_private_site_exclude_ips" rows="10" cols="50" id="dkzr_private_site_exclude_ips" class="large-text code">%s</textarea>', htmlspecialchars( $setting, ENT_QUOTES ) );
  }

  public function setting_field_exclude_uris() {
    $setting = get_option( 'dkzr_private_site_exclude_uris' );

    echo wpautop( __( 'Add one full page URI per line, eg. `/login`. Comments start with "#"', 'dkzr-private-site' ) );
    printf( '<textarea name="dkzr_private_site_exclude_uris" rows="10" cols="50" id="dkzr_private_site_exclude_uris" class="large-text code">%s</textarea>', htmlspecialchars( $setting, ENT_QUOTES ) );
  }

/**
 * Enforce Private Site
 */
  public function wp_loaded() {
    if (
      ( defined( 'WP_CLI' ) && WP_CLI ) ||
      ( defined( 'WP_INSTALLING' ) && WP_INSTALLING ) ||
      $this->check_exclude_ips()
    ) {
      // always allow access based on IPs
      return;
    }

    /**
     * XML-RPC has its own per-method auth system.
     * Unless allowed by IPs we just block all access.
     */
    if ( defined( 'XMLRPC_REQUEST' ) && XMLRPC_REQUEST ) {
      $this->wp_die( false );
    }

    /**
     * AJAX requests must be authenticated
     */
    if ( wp_doing_ajax() && ! is_user_logged_in() ) {
      // no access, display error message
      $this->wp_die( false );
    }

    add_filter( 'rest_authentication_errors', [ $this, 'rest_authentication_errors' ], 0 );
    add_filter( 'wp_robots', [ $this, 'wp_robots'], 999999, 1 );
    add_action( 'template_redirect', [ $this, 'template_redirect' ], 0 );
  }

/**
 * Block /wp-json/ requests on authentication
 */
// TODO: check if get_rest_url() is in excluded_uris?
  public function rest_authentication_errors( $auth ) {
    if ( ! is_user_logged_in() ) {
      // no access, display error message
      return new WP_Error(
        'rest_private_site',
        __( 'This is a private site, please login to access.', 'dkzr-private-site' ),
        [ 'response'  => 401, ]
      );
    } else {
      return $auth;
    }
  }

  public function wp_robots( array $robots ) {
    $robots['noindex'] = true;
    $robots['nofollow'] = true;
    $robots['noarchive'] = true;

    unset( $robots['follow'] );

    return $robots;
  }

/**
 * Block regular users on 'template_redirect' action.
 */
  public function template_redirect() {
    if (
      ! $this->check_frontend_roles() &&
      ! $this->check_exclude_uris()
    ) {
      if ( is_robots() ) {
        // special robots.txt output
        header( 'Content-Type: text/plain; charset=utf-8' );
        $output = "# This is a private site\n";
        $output .= "User-agent: *\n";
        $output .= "Disallow: /\n";
        echo $output;
        exit();
      } else if ( is_favicon() ) {
        wp_die( new WP_Error( 'http_404', __( 'File not found', 'dkzr-private-site' ), ['status' => 404] ) );
        exit();
      } else {
        // no access, display placeholder page
        $this->wp_die();
      }
    }
  }

/**
 * No access, display placeholder page or error message
 */
  protected function wp_die( $add_style = true ) {
    $message =  [ __( 'This is a private site, please login to access.', 'dkzr-private-site' ) ];
    if ( $add_style ) {
      $message[] = sprintf('<style type="text/css">html {background: url(%s) center/cover no-repeat;width:100vw;height:100vh;}</style>', esc_url( plugins_url( '/assets/sky-and-trees-dkzr.jpg', __FILE__ ) ) );
      $message[] = '<!-- About the background image: "Sky and Trees" by DKZR (https://dkzr.nl/) is marked with CC BY-NC-ND 4.0. To view the terms, visit https://creativecommons.org/licenses/by-nc-nd/4.0/. -->';
    }
    wp_die(
     implode( "\n", $message ),
      __( 'Private site', 'dkzr-private-site' ),
      [
        'response'  => 401,
        'code'      => 'rest_private_site',
        'link_url'  => admin_url(),
        'link_text' => __( 'Go to admin', 'dkzr-private-site' ),
      ]
    );
    exit();
  }

  protected function check_frontend_roles() {
    // check super admin (when multisite is activated) / check admin (when multisite is not activated)
    if ( is_super_admin() ) {
      return true;
    }

    $user          = wp_get_current_user();
    $user_roles    = ! empty( $user->roles ) && is_array( $user->roles ) ? $user->roles : [];
    $allowed_roles = array_filter( (array) get_option( 'dkzr_private_site_frontend_roles' ) );

    // add `administrator` role when multisite is activated and the admin of a blog is trying to access his blog
    if ( is_multisite() ) {
      array_push( $allowed_roles, 'administrator' );
    }

    $is_allowed = (bool) array_intersect( $user_roles, $allowed_roles );

    return $is_allowed;
  }

  protected function normalize_ip( $ip ) {
    if ( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) )
      return $ip;
    elseif ( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 ) ) {
      $hex = bin2hex( inet_pton( $ip ) );
      if ( substr( $hex, 0, 24 ) == '00000000000000000000ffff' ) // IPv4-mapped IPv6 addresses
        return long2ip( hexdec( substr( $hex, -8) ) );
      return implode( ':', str_split( $hex, 4 ) );
    }

    return false;
  }


  protected function check_exclude_ips() {
    $ips = array_filter( preg_split( '/[\n\r]+/', get_option( 'dkzr_private_site_exclude_ips' ) ) );
    $remote_address = filter_var( isset( $_SERVER['REMOTE_ADDR'] ) ? $_SERVER['REMOTE_ADDR'] : '', FILTER_VALIDATE_IP );
    $remote_address = $this->normalize_ip( $remote_address );

    if ( empty( $remote_address ) ) {
      return false;
    }

    foreach( $ips as $ip ) {
      if ( false !== strpos( $ip, '#' ) ) {
        $ip = trim( substr( $ip, 0, strpos( $ip, '#' ) ) );
      }
      $ip = $this->normalize_ip( $ip );

      if ( empty( $ip ) ) { // just to be sure :-)
        continue;
      }

      if ( $ip == $remote_address ) {
        return true;
      }
    }

    return false;
  }

  protected function check_exclude_uris() {
    if ( is_404() ) {
      // 404 can't be right...
      return false;
    }

    $uris = array_filter( preg_split( '/[\n\r]+/', get_option( 'dkzr_private_site_exclude_uris' ) ) );
    $request_uri = isset( $_SERVER['REQUEST_URI'] ) ? rawurldecode( parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH ) ?? '' ) : '';
    $request_uri = '/' . ltrim( wp_sanitize_redirect( $request_uri ), '/' );

    $home_path   = trim( parse_url( home_url(), PHP_URL_PATH ) ?? '', '/' );
    foreach( $uris as $uri ) {
      if ( false !== strpos( $uri, '#' ) ) {
        $uri = trim( substr( $uri, 0, strpos( $uri, '#' ) ) );
      }
      $uri = wp_sanitize_redirect( $uri );

      if ( empty( $uri ) ) { // just to be sure :-)
        continue;
      } else {
        $uri = '/' . ltrim( $home_path . '/' . ltrim( $uri, '/' ), '/' );
      }

      if ( preg_match( '#^' . preg_quote( $uri, '#' ) . '/?$#', $request_uri ) ) {
        return true;
      }
    }

    return false;
  }
}
$GLOBALS['dkzr_private_site'] = new dkzrPrivateSite();
